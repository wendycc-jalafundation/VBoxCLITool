## Tools used
- Virtual Box library: VBoxC.dll. (Required to run)
- .Net Framework 4.5.2. (Required to run)
- Virtual Box SDK Docs

## Process Details
Check the <a href="https://gitlab.com/wendycc-jalafundation/VBoxCLITool/-/blob/master/ProcessDetails.md">ProcessDetails.md</a> file to see the detailed explanation of each action.

## Usage
Use the binaries located in VBoxCLITool\bin\debug:
- VBoxCLITool.exe
- Interop.VirtualBox.dll
- VBoxCLITool.exe.config (or app.config)
  
```PowerShell
# Usage
> VirtualBoxCLITool.exe <action> <params>

# Creates a VM from scratch        
> VirtualBoxCLITool.exe create

# Starts the VM with name <MachineName>    
> VirtualBoxCLITool.exe start <MachineName>

# Creates a VM with name <MachineName> from an existing one          
> VirtualBoxCLITool.exe clone <MachineName> <type: ubuntu,fedora>|<name of existing VM>

# Deletes the VM with name <MachineName>
> VirtualBoxCLITool.exe delete <MachineName>
```
## `create`
Creates a VM from scratch. This action uses the .config parameters to create the VM and configure the OS. It also starts the VM to complete the unattended OS installation.

Fill the .config file with the required VM parameters (all mandatory)
```c#     
        <add key="Machine Name" value=""/>
		<add key="OS Type" value="ubuntu_64"/> # or fedora_64
		<add key="VRAM Size" value="16"/> # recommended
		<add key="Memory Size" value="2046"/> 
		<add key="Storage Size" value="10"/> # recommended
		<add key="ISO Path" value="\path\to\iso"/>
		<add key="Username" value=""/>
		<add key="Password" value=""/>
```
### Example:
```PowerShell
> VirtualBoxCLITool.exe create
```
With the `app.config`:
```c#
        <add key="Machine Name" value="TestF1"/>
		<add key="OS Type" value="ubuntu_64"/>
		<add key="VRAM Size" value="16"/>
		<add key="Memory Size" value="2046"/>
		<add key="Storage Size" value="10"/>
		<add key="ISO Path" value="C:\Users\ccwen\Downloads\ubuntu-20.04.3-desktop-amd64.iso"/>
		<add key="Username" value="operator"/>
		<add key="Password" value="operator"/>
```
## `start <MachineName>`
Starts the `MachineName` VM.

### Example:
```
> VirtualBoxCLITool.exe start "BC-DEVOPS-FEDORA35"
```

## `clone <MachineName> <type: ubuntu,fedora>|<name of existing VM>`

Creates a VM from an existing one. For the parameters `ubuntu` or `fedora`, this action will search locally for the VMs `BC-DEVOPS-FEDORA35` or `BC-DEVOPS-UBUNTU22`. 

Or you can use any VM you have locally.
### Examples: 
```
> VirtualBoxCLITool.exe clone "MyNewFedora" fedora
```
or
```
> VirtualBoxCLITool.exe clone "MyNewFedora" "MyExistingFedora"
```
## `delete <MachineName>`

Deletes the VM with name `MachineName`.
### Example:
```
> VirtualBoxCLITool.exe delete "MyNewFedora"
```
