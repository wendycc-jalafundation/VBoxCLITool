# Main Actions
The main methods use in this code are:
```c#
//handles and calls the start() method to start a VM
executeStart(string machineName){...}

//handles and calls the delete() method to delete a VM
executeDelete(string machineName){...}

//handles and calls the createByCloningMachine() method. It expects either "ubuntu" or "fedora" as osType
executeClone(string osType, string machineName){...}

//handles and calls the createByCloningMachine(). It will use an existing VM called baseMachineName
executeClone(string baseMachineName, string newMachineName, bool flag){...}

//handles and calls the create() and unattendedOSInstallation() methods. 
executeCreate()
```
# Core methods

## `create()` method
The following steps describe how to `create` a VM from scratch.

We'll use the following parameters from the app.config file:
  
    - Machine Name
    - VRAM Size
    - Memory Size
    - Storage Size
  
  
1. Create an `IMachine` object using the `IVirtualBox::createMachine` method. Pass the `MachineName` parameter.
2. Set the `MemorySize` property of the new `IMachine` object.
3. Create and assign an `IGraphicsAdapter` object to the new `IMachine` object. Set its `VRAMSize` and `GraphicsControllerType` properties.
4. Use the `IMachine::AddStorageController` method to assign a SATA storage controller.
5. Register the new `IMachine` object to Virtual Box with the method `IVirtualBox::RegisterMachine`.
6. Create a new `IMedium` with type Hard Disk.
7. Write the `IMedium` object in Virtual Box using `IMedium::createBaseStorage`. Pass the `storageSize` parameter.
8. Reopen the registered `IMachine` object in a new `Session`.
9. Attach the `IMedium` object to the `StorageController` of the `IMachine` object.
10. Save de changes with `IMachine::SaveSettings`.
11. Close the `Session` using the `Session::UnlockMachine` method.

## `unattendedOSInstallation()` method

We'll use the following parameters from the app.config file:

    - Machine Name
    - ISO Path
    - Username
    - Password


The following steps describe how to configure the unattended OS installation.
1. Create an `IUnattended` object using  the `IVirtualBox::CreateUnattendedInstaller` method.
2. Assign the properties `IsoPatch`, `Machine`, `Password`, and `User` to the `IUnattended` object.
3. Execute the following methods orderly:
    - `IUnattended::Prepare`
    - `IUnattended::ConstructMedia`
    - `IUnattended::ReconfigureVM`
    - `IUnattended::Done`
  
After completing this procedure we need to execute de `start` action for the OS installation to finish.

## `start()` method
The following steps describe how to `start` an existing VM.

1. Create a `IMachine` object. Assign the machine found by its name using the `IVirtualBox::FindMachine` method.
2. Use the  `IMachine::LaunchVMProcess` to start the VM.

## `createByCloningMachine()` method

The following steps describe how to `clone` an existing VM.

1. Create a `IMachine` object for the VM you want to clone. Assign the machine found by its name using the `IVirtualBox::FindMachine` method.
2. Create a `IMachine` object for the new VM.
3. Use the  `IMachine::CloneTo` to clone the machine from the existing one to the new one.
4. Register the new VM using `IVirtualBox::RegisterMachine` method.

## `delete()` method
The following steps describe how to `delete` an existing VM.

1. Create a `IMachine` object. Assign the machine found by its name using the `IVirtualBox::FindMachine` method.
2. Unregister the machine from VirtualBox using the  `IMachine::Unregister` method. The recommended `CleanupMode` is `DetachAllReturnHardDiskOnly`.
3. Use the result from the previous step and pass it as parameter in the `IMachine::DeleteConfig` method.