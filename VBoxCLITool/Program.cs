﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using VirtualBox;


namespace VBoxCLITool
{
    class Program
    {
        public static string USERPROFILE= Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        static void Main(string[] args)
        {
            
            try
            {
                if (args.Length == 0)
                {

                    printUsage();
                }

                else
                {
                    ExecuteAction(args);
                    switch (args[0])
                    {
                        case "-u":
                            printUsage();
                            break;
                        case "create":
                            Console.WriteLine("Machine created");
                            break;
                        case "clone":
                            Console.WriteLine("Machine cloned");
                            break;
                        case "start":
                            Console.WriteLine("Machine has started");
                            break;
                        case "delete":
                            Console.WriteLine("Machine deleted");
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void printUsage()
        {
            Console.WriteLine("-------------");
            Console.WriteLine("\nUsage:");
            Console.WriteLine("  VirtualBoxCLITool <action> <params>");
            Console.WriteLine("");
            Console.WriteLine("  VirtualBoxCLITool create");
            Console.WriteLine("      Fill the .config file with the required VM parameters (all mandatory)");
            Console.WriteLine("");
            Console.WriteLine("  VirtualBoxCLITool start \"MachineName\"");
            Console.WriteLine("");
            Console.WriteLine("  VirtualBoxCLITool clone \"MachineName\" <type: ubuntu,fedora>|<name of existing VM>");
            Console.WriteLine("");
            Console.WriteLine("  VirtualBoxCLITool delete \"MachineName\"");
           

            return;
        }

        static void ExecuteAction(string[] args)
        {
            string machineName;

            if (args[0] == "start")
            {
                machineName = args[1];
                executeStart(machineName);

            }
            else if (args[0] == "delete")
            {


                executeDelete(args[1]);
            }else if (args[0] == "create")
            {
                executeCreate();
                
            }
            else if (args[2] != null)
            {
                if (args[0] == "clone" && (args[2] == "fedora" || args[2] == "ubuntu"))
                {
                    executeClone(args[2], args[1]);

                }
                else if (args[0] == "clone" && args[2] != "fedora" && args[2] != "ubuntu")
                {

                    executeClone(args[2], args[1], true);
                }
            }
            else
            {
                printUsage();
            }


        }

        private static void executeCreate()
        {

            string machineName = ConfigurationManager.AppSettings["Machine Name"];
            uint memSize = UInt32.Parse(ConfigurationManager.AppSettings["Memory Size"]);
            uint vRamSize = UInt32.Parse(ConfigurationManager.AppSettings["VRAM Size"]);
            string osType = ConfigurationManager.AppSettings["OS Type"];
            long storageSize = Int32.Parse(ConfigurationManager.AppSettings["Storage Size"]);
            storageSize = (((storageSize * 1024) * 1024) * 1024);
            
            Console.WriteLine("Creating machine: " + machineName);
            createVMFromScratch(machineName,memSize,vRamSize,osType,storageSize);

            string password = ConfigurationManager.AppSettings["Password"];
            string username = ConfigurationManager.AppSettings["Username"];
            string isoPath = ConfigurationManager.AppSettings["ISO Path"];
            Console.WriteLine("Configuring OS...");
            unattendedOSInstallation(machineName,isoPath,username,password);
            Console.WriteLine("Starting VM...");
            Console.WriteLine("Wait for the OS installation to complete");
            executeStart(machineName);

        }

        static void executeStart(string machineName)
        {
            start(machineName);
            Console.WriteLine("Machine: " + machineName + " is starting. Please wait...");
            System.Threading.Thread.Sleep(10000);

        }
        
        static void executeClone(string osType, string machineName)
        {
            Console.WriteLine("Creating machine with Type: " + osType + " and Name: " + machineName + ".");
            Console.WriteLine("Please wait...");

            switch (osType)
            {
                case "ubuntu":
                    createByCloningMachine(machineName, "BC-DEVOPS-UBUNTU22");
                    break;
                case "fedora":
                    createByCloningMachine(machineName, "BC-DEVOPS-FEDORA35");
                    break;
                default:
                    break;
            }
            System.Threading.Thread.Sleep(10000);
        }
        
        static void executeClone(string baseMachine, string machineName, bool flag)
        {
            Console.WriteLine("Creating machine with Name: " + machineName + "using Base Machine: " + baseMachine);
            Console.WriteLine("Please wait...");

            createByCloningMachine(machineName, baseMachine);
            System.Threading.Thread.Sleep(10000);
        }
        
        static void executeDelete(string machineName)
        {
            Console.WriteLine("Deleting machine: " + machineName + ". Please wait...");
            delete(machineName);
        }

        static void delete(string machineName)
        {
            IVirtualBox virtualBox = new VirtualBox.VirtualBox();

            IMachine targetMachine = virtualBox.FindMachine(machineName);
            targetMachine.DeleteConfig(targetMachine.Unregister(CleanupMode.CleanupMode_DetachAllReturnHardDisksOnly));

        }
        
        static void start(string machineName)
        {
            IVirtualBox virtualBox = new VirtualBox.VirtualBox();
            IMachine targetMachine = virtualBox.FindMachine(machineName);

            Session session = new Session();
            string[] environmentChanges = new string[0];
            targetMachine.LaunchVMProcess(session, "", environmentChanges);

        }

        static void createByCloningMachine(string machineName, string baseMachineName)
        {
            IVirtualBox virtualBox = new VirtualBox.VirtualBox();
            IMachine baseMachine = virtualBox.FindMachine(baseMachineName);

            IMachine newMachine = virtualBox.CreateMachine("", machineName, null, baseMachine.OSTypeId, Guid.NewGuid().ToString());
            CloneOptions[] cloneOptions = { CloneOptions.CloneOptions_KeepHwUUIDs };
            baseMachine.CloneTo(newMachine, CloneMode.CloneMode_MachineState, cloneOptions);

            virtualBox.RegisterMachine(newMachine);
        }

        static void createVMFromScratch(string machineName, uint memSize, uint vRamSize, string osType, long storageSize)
        {
            
            string uuid = Guid.NewGuid().ToString();
            IVirtualBox virtualBox = new VirtualBox.VirtualBox();
            IMachine newMachine = virtualBox.CreateMachine("", machineName, null, osType, uuid);
            newMachine.MemorySize = memSize;
            IGraphicsAdapter adapter = newMachine.GraphicsAdapter;
            adapter.GraphicsControllerType = GraphicsControllerType.GraphicsControllerType_VMSVGA;
            adapter.VRAMSize = vRamSize;
            newMachine.AddStorageController("AHCI", StorageBus.StorageBus_SATA);

            virtualBox.RegisterMachine(newMachine);

            IMedium medium = virtualBox.CreateMedium("",USERPROFILE + "\\VirtualBox VMs\\" + machineName + "\\" + machineName + ".vdi",AccessMode.AccessMode_ReadWrite,DeviceType.DeviceType_HardDisk );
            MediumVariant[] variant = { MediumVariant.MediumVariant_Standard };
            
            medium.CreateBaseStorage(storageSize, variant);
            System.Threading.Thread.Sleep(20000);

            IMedium m = virtualBox.OpenMedium(USERPROFILE+"\\VirtualBox VMs\\" + machineName + "\\" + machineName + ".vdi", DeviceType.DeviceType_HardDisk, AccessMode.AccessMode_ReadWrite, 0);
            Console.WriteLine(m.State.ToString());
            
            Session sessionMachine = new Session();
            newMachine.LockMachine(sessionMachine, LockType.LockType_Write);
            IMachine mutable = sessionMachine.Machine;

            mutable.AttachDevice("AHCI", 0, 0, DeviceType.DeviceType_HardDisk, m);
            mutable.SaveSettings();
            sessionMachine.UnlockMachine();

        }

        static void unattendedOSInstallation(string machineName, string isoPath, string username, string password)
        {
            IVirtualBox virtualBox = new VirtualBox.VirtualBox();
            IUnattended unattended = virtualBox.CreateUnattendedInstaller();
            unattended.IsoPath= isoPath;
            IMachine machine = virtualBox.FindMachine(machineName);
            unattended.Machine = machine;
            unattended.Password = password;
            unattended.User = username;
            unattended.Prepare();
            unattended.ConstructMedia();
            unattended.ReconfigureVM();
            unattended.Done();


        }


    }
}
